//
//  ViewController.swift
//  FBReaction
//
//  Created by Banwari Patidar on 01/06/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tbl:UITableView!
     
    var img:[UIImage] = [
    UIImage(named: "icn_angry")!,
    UIImage(named: "icn_laugh")!,
    UIImage(named: "icn_like")!,
    UIImage(named: "icn_love")!,
    UIImage(named: "icn_sad")!
    ]
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    var myCollectionView:UICollectionView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbl.delegate = self
        tbl.dataSource = self
        
        prepareColl()
     
    }
    
    func prepareColl() {
       
        
      
       
        //var conterView = UIView()
        
    }


}

extension ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TBCell") as! TBCell
        cell.btn.tag = indexPath.row
        cell.btn.addTarget(self, action: #selector(btnCLick(_:event:)), for: .touchUpInside)
        
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        cell.btn.addGestureRecognizer(longPressGesture)
            
        return cell
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        
            if gestureRecognizer.state == .began {
                // Handle the long press event here
                print("Button long pressed!")
                myCollectionView?.removeFromSuperview()
             let loc = gestureRecognizer.location(in: view)
                print("view location:",loc)
                myCollectionView = UICollectionView(frame: CGRect(x: loc.x, y: loc.y, width: 250, height: 55), collectionViewLayout: layout)
               
                   layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
                   layout.itemSize = CGSize(width: 37, height: 37)
                layout.scrollDirection = .horizontal
                myCollectionView!.allowsSelection = true
                myCollectionView!.isUserInteractionEnabled = true
                myCollectionView!.register(CollCell.self, forCellWithReuseIdentifier: "MyCell")
                myCollectionView!.backgroundColor = UIColor.white
                myCollectionView!.layer.cornerRadius = myCollectionView!.frame.height / 2
                myCollectionView!.clipsToBounds = true
                myCollectionView!.dataSource = self
                myCollectionView!.delegate = self
                view.addSubview(myCollectionView!)
            }
//        if gestureRecognizer.state == .ended {
//            myCollectionView?.removeFromSuperview()
//        }
        }
    
    @objc func btnCLick(_ Sender: UIButton, event: UIEvent){
        print("Button clicked ")
    }
    
}

extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath)
        
        let imgV = UIImageView(image: img[indexPath.row] )
        imgV.frame = myCell.contentView.frame
        myCell.contentView.addSubview(imgV)
        return myCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("User tapped on item \(indexPath.row)")
        myCollectionView?.removeFromSuperview()
    }
    
}

  

class TBCell: UITableViewCell{
    @IBOutlet weak var btn:UIButton!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }
}

class CollCell: UICollectionViewCell {
    var imf = UIImage()
    override init(frame: CGRect) {
        super.init(frame: frame)
  
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


 


